package com.sda.back.example.controller;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;
import com.sda.back.example.service.DataServiceImpl;

import java.util.List;

/**
 * Created by DARJ on 14.01.2018.
 */
public class DataController {
    private DataServiceImpl dataService = new DataServiceImpl();

    public void saveData(List<User> existingUsers, List<Task> listOfTasks)
    {
        dataService.saveData(existingUsers, listOfTasks);
    }
    public void loadData(List<User> existingUsers, List<Task> listOfTasks)
    {
        dataService.loadData();
    }
}
