package com.sda.back.example.controller;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;
import com.sda.back.example.service.TaskServiceImpl;

import java.util.List;

/**
 * Created by DARJ on 13.01.2018.
 */
public class TaskController {
    private TaskServiceImpl taskService = new TaskServiceImpl();


    public void createTask(String name, User user)
    {
       if (taskService.createTask(name, user)){
           System.out.println("Task has been created");
       }
       else
           System.err.println("Task already exists. Can't be created");
    }
    public void closeTask(String name, User user)
    {
        if (taskService.closeTask(name, user))
        {
            System.out.println("Task \"" + name + "\" has been closed");
        }
        else
        {
            System.err.println("Task " + name + " can't be closed, or doesn't exist");
        }
    }
    public void printListOfTasks()
    {
        List<Task> taskList = taskService.getTaskList();
        for (Task task: taskList) {
            if (task.isClosed()) {
                System.out.println("- " + task.getTaskName() + " assigned to: " + task.getAssignedTo().getLogin() +
                        " Status: Closed");
            }
            else {
                System.out.println("- " + task.getTaskName() + " assigned to: " + task.getAssignedTo().getLogin() +
                        " Status: In progress");
            }
        }
        System.out.println("\n Total number of tasks is: " + taskList.size());
    }

    public void printListOfAssignedTasks(User user) {
        taskService.printListOfAssignedTasks(user);
        }

    public List<Task> getListOfAllTasks() {
        return taskService.getTaskList();
    }
}

