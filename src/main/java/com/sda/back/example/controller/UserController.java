package com.sda.back.example.controller;

import com.sda.back.example.model.User;
import com.sda.back.example.service.UserService;
import com.sda.back.example.service.UserServiceImpl;

import java.util.List;

public class UserController {

    private UserService userService = new UserServiceImpl();

    // Odpowiada za odpowiednie wywołanie zadania na serwisie, oraz wyświetlenie odpowiedzi

    public void registerUser(String login, String password){
        if(userService.registerUser(login, password)){
            System.out.println("Udało Ci się zarejestrować, Witamy!");
        }else{
            System.err.println("Nie udało się zarejestrować :(");
        }
    }
    public void logIn(String login, String password)
    {
        if (userService.logIn(login,password))
        {
            System.out.println("User with " + login +" managed to log in");
        }
        else
        {
            System.out.println("Username or password is not valid, or the user does not exist");
        }
    }
    public void logOff(String login)
    {
        if (userService.logOff(login))
        {
            System.out.println("User " + login + " managed to log off");
        }
        else
        {
            System.out.println("User is not logged in");
        }
    }

    public List<User> getListOfLoggedUsers()
    {
        return userService.getLoggedUsersList();
    }
    public List<User> getListOfExistingUsers()
    {
        return userService.getExistingUsersList();
    }
    public void printListOfLoggedUsers()
    {
        List<User> listOfLoggedUsers = getListOfLoggedUsers();
        System.out.println("List of logged users: ");
        for (User user: listOfLoggedUsers) {
            System.out.println("- " + user.getLogin());
        }
    }
    public void printListOfExistingUsers()
    {
        List<User> listOfExistingUsers = getListOfExistingUsers();
        System.out.println("List of existing users in the system: ");
        for (User user: listOfExistingUsers) {
            System.out.println("- " + user.getLogin());
        }
    }

}
