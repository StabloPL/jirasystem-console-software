package com.sda.back.example.model;

import java.time.LocalDate;

/**
 * Created by DARJ on 13.01.2018.
 */
public class Task {
    private long taskID;
    private String taskName;
    private String description;
    private boolean isClosed;
    private LocalDate startDate;
    private LocalDate endDate;
    private User assignedTo;

    public Task(long taskID, String taskName,String description, LocalDate startDate, LocalDate endDate, User assignedTo) {
        this.taskID = taskID;
        this.taskName = taskName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isClosed = isClosed;
        this.assignedTo = assignedTo;
        this.description=description;
    }

    public Task(long taskID, String taskName) {
        this.taskID = taskID;
        this.taskName = taskName;
    }
    public Task(long taskID, String taskName, User assignedTo) {
        this.taskID = taskID;
        this.taskName = taskName;
        this.assignedTo = assignedTo;
    }


    public long getTaskID() {
        return taskID;
    }

    public void setTaskID(long taskID) {
        this.taskID = taskID;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public User getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(User assignedTo) {
        this.assignedTo = assignedTo;
    }

    @Override
    public String toString() {
        return taskID+","+taskName+","+description+","+isClosed;
       // return taskID+","+taskName+","+description+","+isClosed+","+startDate+","+endDate+","+assignedTo;
    }
}
