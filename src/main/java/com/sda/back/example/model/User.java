package com.sda.back.example.model;

import java.util.ArrayList;
import java.util.List;

public class User {
    private long id;
    private String name;
    private String login;
    private String passwordHash;
    private boolean isLogged;
    private List<Task> listOfAssignedTasks;

    public User() {
    }

    public User(long id, String login, String passwordHash) {
        this.id = id;
        this.login = login;
        this.passwordHash = passwordHash;
        this.listOfAssignedTasks = new ArrayList<>(); {
        }
    }

    public boolean isLogged() {
        return isLogged;
    }

    public void setLogged(boolean logged) {
        isLogged = logged;
    }

    public User(long id, String name, String login, String passwordHash) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }


    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public List<Task> getListOfAssignedTasks() {
        return listOfAssignedTasks;
    }

    public void setListOfAssignedTasks(List<Task> listOfAssignedTasks) {
        this.listOfAssignedTasks = listOfAssignedTasks;
    }

    @Override
    public String toString() {
        return (id +","+name+","+login+","+passwordHash+","+isLogged+","+listOfAssignedTasks);
    }
}

