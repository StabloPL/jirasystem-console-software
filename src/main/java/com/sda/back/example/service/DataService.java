package com.sda.back.example.service;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;

import java.util.List;

/**
 * Created by DARJ on 14.01.2018.
 */
public interface DataService {
    public void loadData();
    public void saveData(List<User> existingUsers, List<Task> listOfTasks);
}
