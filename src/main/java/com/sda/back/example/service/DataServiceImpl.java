package com.sda.back.example.service;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;

import java.io.*;
import java.util.List;

/**
 * Created by DARJ on 14.01.2018.
 */
public class DataServiceImpl implements DataService {


    @Override
    public void loadData() {
    // TODO: Whole loading
    }

    @Override
    public void saveData(List<User> existingUsers, List<Task> listOfTasks) {
        // TODO: Make possible to save Lists
        File file = new File("savedFile.txt");
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.append("Users:\n");
            for (User user: existingUsers) {
                writer.append(user.toString() + "\n");
            }
            writer.append("\n");
            writer.append("Tasks:\n");
            for (Task task: listOfTasks) {
                writer.append(task.toString() + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }





}
