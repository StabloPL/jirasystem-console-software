package com.sda.back.example.service;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;

import java.util.List;

/**
 * Created by DARJ on 13.01.2018.
 */
public interface TaskService {
    public boolean doTaskExists(String name);
    public boolean createTask(String name, User user);
    public boolean deleteTask(String name, User user);
    public Task getTask(String name);
    public long returnExistingTaskID(String name);
    public boolean closeTask(String name, User user);
    public List<Task> getTaskList();
    void printListOfAssignedTasks(User user);

}
