package com.sda.back.example.service;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;

import java.util.*;

/**
 * Created by DARJ on 13.01.2018.
 */
public class TaskServiceImpl implements TaskService {
    private static long TASK_ID = 1;
    private Map<Long, Task> taskMap = new HashMap<>();


    @Override
    public boolean doTaskExists(String name) {
        List<Task> list = new ArrayList<Task>(taskMap.values());
        for (Task task : list) {
            if (task.getTaskName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public long returnExistingTaskID(String name) {
        if (doTaskExists(name)) {
            List<Task> list = new ArrayList<Task>(taskMap.values());
            for (Task task : list) {
                if (task.getTaskName().equalsIgnoreCase(name)) {
                    return task.getTaskID();
                }
            }
        }
        return 0;
    }

    @Override
    public boolean closeTask(String name, User user) {
        if (doTaskExists(name)) {
            List<Task> listOfAssignedTasks = user.getListOfAssignedTasks();
            for (Task task : listOfAssignedTasks) {
                if (getTask(name).getTaskName().equals(task.getTaskName())) {
                    long taskID = returnExistingTaskID(name);
                    taskMap.get(taskID).setClosed(true);
                    return true;
                }
            }
        }
        return false;
    }
        @Override
        public List<Task> getTaskList () {
            List<Task> list = new ArrayList<>(taskMap.values());
            return list;
        }


        @Override
        public boolean createTask(String name, User user){
            if (doTaskExists(name)) {
                return false;
            }
                    Task task = new Task(TASK_ID, name);
                    taskMap.put(TASK_ID++, task);
                    task.setAssignedTo(user);
                    user.getListOfAssignedTasks().add(task);
                    return true;

        }

        @Override
        public boolean deleteTask (String name, User user){
            if (doTaskExists(name)) {
                long taskID = returnExistingTaskID(name);
                taskMap.remove(taskID);
                return true;
            }
            return false;
        }

        @Override
        public Task getTask (String name){
            if (doTaskExists(name)) {
                return taskMap.get(returnExistingTaskID(name));
            }
            return null;
        }

        @Override
        public void printListOfAssignedTasks (User user){
            List<Task> listOfAssignedTasks = user.getListOfAssignedTasks();
            System.out.println("List of assigned tasks to " + user.getLogin() + " :");
            for (Task task : listOfAssignedTasks) {
                if (task.isClosed()) {
                    System.out.println("- " + task.getTaskName() + " Status is: Closed" );
                }
                else
                {
                    System.out.println("- " + task.getTaskName() + " Status is: In progress" );
                }
            }
        }
    }

