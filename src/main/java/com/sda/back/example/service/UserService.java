package com.sda.back.example.service;

import com.sda.back.example.model.User;

import java.util.List;

public interface UserService {
    User getUserWithId(long id);
    boolean registerUser(String login, String password);
    boolean userExists(String login);
    boolean logIn(String login, String password);
    boolean logOff(String login);
     List<User> getLoggedUsersList();
     List<User> getExistingUsersList();
}
