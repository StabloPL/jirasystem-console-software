package com.sda.back.example.service;

import com.sda.back.example.model.User;
import org.apache.commons.codec.digest.DigestUtils;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserServiceImpl implements UserService {

    private static long USER_ID_COUNTER = 1;
    //    private List<User> users = new ArrayList<>();
    private Map<Long, User> users = new HashMap<>(); // mapa id -> user
    private List<User> loggedUsersList = new ArrayList<>();

    public User getUserWithId(long id) {
        return users.get(id);
    }

    public boolean registerUser(String login, String password) {
        if (userExists(login)) {
            return false;
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");

            String hashed = DigestUtils.md5Hex(password).toLowerCase();


            System.out.println("Encrypted password is: " + hashed);

            User newUser = new User(USER_ID_COUNTER++, login, password);

            users.put(newUser.getId(), newUser);
            return true;
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Unable to encrypt password - no such algorithm.");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean userExists(String login) {
        List<User> allUsers = new ArrayList<>(users.values());

        for (User user: allUsers) {
            if (user.getLogin().equals(login))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean logIn(String login, String password) {
        List<User> listOfUsers = new ArrayList<>(users.values());
        for (User user: listOfUsers){
            if (user.getLogin().equalsIgnoreCase(login) && user.getPasswordHash().equals(password))
            {
                user.setLogged(true);
                loggedUsersList.add(user);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean logOff(String login) {
        for (User user: loggedUsersList) {
            if (user.getLogin().equalsIgnoreCase(login))
            {
                if (user.isLogged())
                {
                    user.setLogged(false);
                    loggedUsersList.remove(user);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }

    public List<User> getLoggedUsersList() {
        return loggedUsersList;
    }
    public List<User> getExistingUsersList(){
        List<User> listOfExistingUsers = new ArrayList<>(users.values());
        return listOfExistingUsers;
    }
}
