package com.sda.back.example.views;


import com.sda.back.example.controller.DataController;
import com.sda.back.example.controller.TaskController;
import com.sda.back.example.controller.UserController;
import com.sda.back.example.model.User;
import com.sda.back.example.service.DataServiceImpl;

public class CommandParser {
    private UserController userController = new UserController();
    private TaskController taskController = new TaskController();
    private DataController dataController = new DataController();

    public void parseLine(String line) {
        String[] words = line.trim().split(" ");
        if (words[0].toLowerCase().equals("user")) {
            parseUserCommand(words);
        }
        else if (words[0].toLowerCase().equals("task")){
            //  parseTaskCommand(words);
            System.out.println("Tasks can be only created by logged users");
         }
        else if (words[0].toLowerCase().equals("quit")){
            //  parseTaskCommand(words);
            System.out.println("Savind data, and closing software...");
            saveData();
        }
        else
        {
            parseLoggedUserCommand(words);
        }
    }

    private void parseLoggedUserCommand(String[] words) {
        try {
            for (User user : userController.getListOfLoggedUsers()) {
                if (words[0].equals(user.getLogin())) {
                    if (words[1].equalsIgnoreCase("task")) {
                        parseTaskCommand(deleteFirstWordFromStringArray(words), user);
                        return;
                    } else if (words[1].equalsIgnoreCase("user")) {
                        parseUserCommand(deleteFirstWordFromStringArray(words));
                        return;
                    } else if (words[1].equalsIgnoreCase("logOff")) {
                        userController.logOff(user.getLogin());
                        return;
                    }
                }
            }
            System.out.println("User is not logged in");
        }
        catch (RuntimeException re)
        {
            System.out.println("It's not proper command");
        }
    }

    private void parseTaskCommand(String[] words, User user) {
        try {
            if (words[1].equalsIgnoreCase("close")) {
                taskController.closeTask(words[2], user);
            } else if (words[1].equalsIgnoreCase("create")) {
                taskController.createTask(words[2], user);
            } else if (words[1].equalsIgnoreCase("printAll")) {
                taskController.printListOfTasks();
            } else if (words[1].equalsIgnoreCase("printAssignedTasks")) {
                taskController.printListOfAssignedTasks(user);
            }
        }
        catch (RuntimeException re)
        {
            System.out.println("It's not proper command");
        }
    }

    private void parseUserCommand(String[] words) {
        try {
            if (words[1].equalsIgnoreCase("register")) {
                userController.registerUser(words[2], words[3]);
            } else if (words[1].equalsIgnoreCase("login")) {
                userController.logIn(words[2], words[3]);
            } else if (words[1].equalsIgnoreCase("printLogged")) {
                userController.printListOfLoggedUsers();
            } else if (words[1].equalsIgnoreCase("printExisting")) {
                userController.printListOfExistingUsers();
            }
        }
        catch (RuntimeException re)
            {
                System.out.println("It's not proper command");
            }


    }
    private String[] deleteFirstWordFromStringArray(String[] words)
    {
        String[] stringArrayToReturn = new String[words.length];
        for (int i = 1; i < words.length; i++) {
            stringArrayToReturn[i-1] = words[i];
        }
        return stringArrayToReturn;
    }

    private void saveData() {
        dataController.saveData(userController.getListOfExistingUsers(),taskController.getListOfAllTasks());
    }
}
