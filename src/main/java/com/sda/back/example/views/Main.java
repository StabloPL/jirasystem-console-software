package com.sda.back.example.views;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Help help = new Help();
        CommandParser parser = new CommandParser();

        System.out.println("Welcome to task management system, available command are: \n");
        System.out.println("help will make this menu re-appear");
        help.printHelp();

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.equalsIgnoreCase("quit")) {
                parser.parseLine(line);
                break;
            } else if (line.equalsIgnoreCase("help")){
                help.printHelp();
            }
             else{
                parser.parseLine(line);
            }
        }
    }
}
